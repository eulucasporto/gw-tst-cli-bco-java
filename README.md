# gw-tst-cli-bco-java

- Teste de Habilidades - Desenvolvedor Java Júnior

Bem-vindo ao teste de habilidades para desenvolvedores Java Júnior no contexto do setor bancário. Este teste é projetado para avaliar seu conhecimento e experiência nas seguintes tecnologias: Spring Boot, JDBC Template, Spring Data JPA, Oracle, Spring Batch, Backbone.js, Spring Security e Jasper Studio.

- Para realizar este teste, você precisará completar as seguintes tarefas:

## 1. Configuração do Projeto:
    - Crie um novo projeto Spring Boot utilizando o Spring Initializr.
    - Configure o projeto para utilizar o banco de dados Oracle.
    - Configure o projeto para usar o Spring Data JPA.

## 2. Modelagem de Dados:
    - Crie as entidades necessárias para representar as operações bancárias, como Conta, Cliente, Transação, etc.
    - Defina as associações adequadas entre as entidades.
    - Utilize as anotações do Spring Data JPA para mapear as entidades para o banco de dados.

## 3. Operações CRUD:
    - Implemente as operações CRUD (Create, Read, Update, Delete) para as entidades definidas.
    - Utilize o JDBC Template do Spring para realizar as operações com o banco de dados.
    - Utilize o Spring Data JPA para as operações mais complexas.

## 4. Integração Bancária com Spring Batch:
    - Implemente um job do Spring Batch para processar arquivos de integração bancária.
    - O job deve ler um arquivo no formato especificado, processar as transações bancárias e atualizar as contas correspondentes.
    - Utilize o Spring Batch para ler e processar o arquivo.
    - Utilize o JDBC Template ou o Spring Data JPA para atualizar as contas no banco de dados.

## 5. Interface do Usuário:
    - Crie páginas HTML usando o Backbone.js para as principais funcionalidades do sistema bancário, como exibir informações da conta, criar transações, etc.
    - Implemente os controladores do Spring MVC para mapear as requisições HTTP e renderizar as páginas HTML.
    - Utilize os serviços criados anteriormente para acessar e manipular os dados.

## 6. Autenticação e Autorização:
    - Implemente a autenticação e autorização utilizando o Spring Security.
    - Crie um sistema de login com autenticação baseada em formulário.
    - Restrinja o acesso às páginas e recursos do sistema com base nos papéis do usuário.

## 7. Relatórios com Jasper Studio:
    - Crie relatórios personalizados utilizando o Jasper Studio.
    - Integre os relatórios com o projeto Spring Boot.
    - Implemente uma funcionalidade para gerar relatórios com base nos dados do sistema bancário.

## Instruções para o teste:
- Crie um repositório GitLab para o seu projeto e forneça o link no Readme.
- Certifique-se de que o projeto esteja bem estruturado e siga as melhores práticas de desenvolvimento.
- Forneça instruções claras sobre como configurar e executar o projeto em um ambiente local.
- Comente o código adequadamente para facilitar a compreensão.

## Requisitos adicionais:
- Utilize as versões mais recentes das tecnologias mencionadas.
- Certifique-se de que o projeto seja executável sem erros.
- Faça o uso adequado de exceções e validações.
- Utilize as melhores práticas de desenvolvimento e padrões de projeto.

Boa sorte! Estamos ansiosos para ver o seu código e avaliar suas habilidades como desenvolvedor Java Júnior no setor bancário.
